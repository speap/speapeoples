+++
date = "2020-05-08"
title = "Bruno Paul Latour"
draft = false
image = "img/portfolio/bruno-paul-latour.jpg"
showonlyimage = true
weight = 2
+++

<!-- DON'T MODIFY OR REPLACE THE LINES ABOVE, THANK YOU !-->
<!-- DON'T MODIFY THE EXTENSION OF THIS FILE, THANK YOU !-->
<!-- DON'T CONVERT THIS FILE INTO A PDF, DOCX or in any other file-format, THANK YOU !-->
<!-- DON'T GET MAD AT ME BECAUSE IT IS TOO COMPLICATED (THIS IS JUST HOW IT WORKS). WE CAN HELP YOU, THANK YOU !-->

---

<!-- If you want to add a picture here just replace my name in the line below as well as the name of my picture "romain-di-vozzo.jpg" by your own picture -->
![Bruno Paul Latour](https://gitlab.com/speap/speapeoples/-/raw/master/static/img/portfolio/bruno-paul-latour.jpg)

<!-- For example, if you had a picture named "bruno-paul-latour.jpg", your link would look like this:
![Bruno Paul Latour](https://gitlab.com/speap/speapeoples/-/raw/master/static/img/portfolio/bruno-paul-latour.jpg) -->

<!-- IMPORTANT: Make sure to send me a square picture of yourself of 500x500 pixels along with this file. Its name should be like my picture: FirstName-LastName.jpg. -->

---

<!-- The titles of the fields below are suggestions. You can modify or remove them as you wish.-->
<!--Just make sure to not remove the " # " or the ``` ", or the " --- ". -->


---

###### ```How and why I ended up applying to SPEAP ? ```
###### blablabla (replace text by your own comments)
###### blobloblo (replace text by your own comments)
###### bliblibli (replace text by your own comments)

---

###### ```How being a SPEAP-student modified the way I work and live today```
###### blablabla (replace text by your own comments)
###### blobloblo (replace text by your own comments)
###### bliblibli (replace text by your own comments)
---

###### ```Current Position(s):```
###### blablabla (replace text by your own comments)
###### blobloblo (replace text by your own comments)
###### bliblibli (replace text by your own comments)

---

###### ```Short Curriculum:```
###### blablabla (replace text by your own comments)
###### blobloblo (replace text by your own comments)
###### bliblibli (replace text by your own comments)

---

###### ```My skills are (but are not limited to):```
###### blablabla (replace text by your own comments)
###### blobloblo (replace text by your own comments)
###### bliblibli (replace text by your own comments)

---

###### ```I often collaborate with:```
###### blablabla (replace text by your own comments)
###### blobloblo (replace text by your own comments)
###### bliblibli (replace text by your own comments)


---

###### ```I am interested in```
###### blablabla (replace text by your own comments)
###### blobloblo (replace text by your own comments)
###### bliblibli (replace text by your own comments)


---

###### ```Email:```
###### Replace this text by your email

---

###### ```Phone Number:```
###### Replace this text by your email

---

###### ```My Website(s)```
###### Replace this text by the link to your website
###### Replace this text by the link to  your website
###### Replace this text by the link to  your website

---

###### ```What I do```
###### Explain what you do
