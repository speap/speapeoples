+++
date = "2020-05-08"
title = "Romain Di Vozzo"
draft = false
image = "img/portfolio/romain-di-vozzo.jpg"
showonlyimage = true
weight = 1
+++


<!-- DON'T MODIFY OR REPLACE THE LINES ABOVE, THANK YOU !-->

---

<!-- If you want to add a picture here just replace my name in the line below as well as the name of my picture "romain-di-vozzo.jpg" by your own picture -->
![Romain Di Vozzo](https://gitlab.com/speap/speapeoples/-/raw/master/static/img/portfolio/romain-di-vozzo.jpg)
<!-- For example, if you had a picture named "bruno-paul-latour.jpg", your link would look like this:
![Bruno Paul Latour](https://gitlab.com/speap/speapeoples/-/raw/master/static/img/portfolio/bruno-paul-latour.jpg) -->
<!-- IMPORTANT: Make sure to send me a square picture of yourself of 500x500 pixels. Its name should be like my picture: FirstName-LastName.jpg. -->

---

<!-- The titles of the fields below are suggestions. You can modify or remove them as you wish.-->
<!--Just make sure to not remove the " # " or the ``` ", or the " --- ". -->
###### ```How and why I ended up applying to SPEAP ? ```
###### <!-- replace this text by your own comments on how and why you ended up applying to SPEAP.  Remove the whole paragraph if not relevant.-->


---

###### ```How being a SPEAP-student has modified the way I work today ?```
###### - <!-- blablabla. replace this text by your own comments on how becoming a SPEAP student modified the way you live and work today.  Remove the whole paragraph if not relevant.-->
###### -
###### -

---

###### ```Current Position(s):```
###### - <!-- blablabla. replace this text by your own comments.  Remove the whole paragraph if not relevant.-->
###### -
###### -

---

###### ```Short Curriculum:```
###### - <!-- blablabla. replace this text by your own comments.  Remove the whole paragraph if not relevant.-->
###### -
###### -

---

###### ```My skills are (but are not limited to):```
###### - <!-- blablabla. replace this text by your own comments.  Remove the whole paragraph if not relevant.-->
###### -

---

###### ```I often collaborate with:```
###### - <!-- blablabla. replace this text by your own comments.  Remove the whole paragraph if not relevant.-->
###### -

---

###### ```I am interested in:```
###### - <!-- blablabla. replace this text by your own comments.  Remove the whole paragraph if not relevant.-->
###### -


---

###### ```Email:```
###### <!--Replace this text by your email-->

---

###### ```Phone Number:```
###### <!--Replace this text by your phone number-->

---

###### ```My Website(s)```
###### <!-- Replace this text by the link to your website.--> For example: [Fablab Digiscope](https://fablabdigiscope.gitlab.io)
###### -
###### -

---

###### ```What I do```
###### <!--Explain what you do if you want to.-->



<!--
---

###### ```How and why I ended up applying to SPEAP ? ```
###### I was feeling trapped into both representations of the artist and the environmental activist I was. In parallel, I couldn't find any political party I could align with. SPEAP seemed to offer a
###### I needed to connect bits of knowledge together in collaboration with other experts/peers who were also questioning their own practices.
###### I needed direct interactions with contemporary and active thinkers.
###### I needed a new context where I could dedicate time to explore new and experimental methodologies and practices.

---

###### ```How being a SPEAP-student modified the way I work and live today```
###### One of the good side-effects of my participation to SPEAP has been the redefinition of where I was standing as an artist, as a person, and as a citizen.
###### I don't define myself as an artist anymore, neither as a Designer even if this terminology seems to define quite well what I currently do. I only need titles so that people I need to interact with understand better where I am stand.
###### I bearly manage to fit what I do into categories. Actually, not being too trapped into categories is a chance I value as much as freedom.
###### I can say for sure that since then - and as a consequence of participating to SPEAP - I became an "Anti-disciplinarity Agent": Meaning that I was able to align with the fact that what I am is always more than the sum of the skills and knowledges I have developed.

---

###### ```Current Position(s):```
###### Director at Fablab Digiscope (France)
###### Fablab Project Manager at Université Paris-Saclay (France)
###### Global Fablab Network Mentor/Guru (WorldWide)

---

###### ```Curriculum:```
###### I started my professional life as a visual-artist
###### I am a former SPEAP student from year 2012-2013
###### I am a former Fab Academy student from year 2012
###### I am a former Bio Academy student from year 2015
###### I am a former Fabricademy student from year 2018-2019
###### I was Global Lead for the coordination of FAB14, the annual gathering of the Global Fablab Network (2018)
###### See more at https://www.linkedin.com/in/romaindivozzo/

---

###### ```My skills are (but are not limited to):```
###### R&D Strategy
###### Design de
###### Design applied to Digital Fabrication
###### Digital Fabrication Processes and Technics for Science Research
###### Curriculum Designer
###### Community Management

---

###### ```I often collaborate with:```
###### The Global Fablab Network
###### The Fab Foundation
###### Neil Gershenfeld and the CBA/MIT

---

###### ```I am interested in```
###### Space Technologies
###### Environmental
###### Ressources Scarcity and Ressources Rarefaction
###### Circular Fabrication
###### Open Harware Advocacy
###### Distributed Networks
###### Modularity

---

###### ```Email:```
###### romain.di-vozzo@universite-paris-saclay.fr

---

###### ```Phone Number:```
###### +33695303826 (SMS or Telegram only - No Calls please)

---

###### ```What I do```
###### I lead Fablab Digiscope since 2013, where I develop cutting-edge research.
